# 桌游-星期五

> **注意**  本项目已转交此处维护：[https://github.com/WhiteRobe/BoardGame-Friday](https://github.com/WhiteRobe/BoardGame-Friday)

#### 项目介绍
《桌游-星期五》是一个基于Vue及其插件、Bootstrap、jQuery的单页面应用项目。

摸鱼的时候和朋友一起写出来的，由于是三个人写，加上对游戏规则的认知不同，反正没有用比较科学的方式写项目。
不过能玩就行。

#### 游戏运行在gitee.io
[点此可进行游戏](https://shenpibaipao.gitee.io/board_game_friday/)(建议使用Chrome)

io地址：[https://shenpibaipao.gitee.io/board_game_friday/](https://shenpibaipao.gitee.io/board_game_friday/)

#### 服务器搭建
参见[>此处<](https://blog.csdn.net/shenpibaipao/article/details/81326552)